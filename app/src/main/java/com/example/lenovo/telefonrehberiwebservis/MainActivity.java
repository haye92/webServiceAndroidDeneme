package com.example.lenovo.telefonrehberiwebservis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConnectionTask task = new ConnectionTask();

        String[] params = new String[1];
        params[0] = "listele";
        task.execute(params);


        Button b = (Button)findViewById(R.id.button2);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionTask task = new ConnectionTask();
                EditText isimText = (EditText) findViewById(R.id.editText);
                EditText numaraText = (EditText) findViewById(R.id.editText2);

                String isim = isimText.getText().toString();
                String numara = numaraText.getText().toString();

                String[] params = new String[3];
                params[0] = "ekle";
                params[1] = isim;
                params[2] = numara;
                task.execute(params);
            }
        });
    }





    private void JSONGoster (String jsonArray) {
        try {

            JSONArray jArray = new JSONArray(jsonArray);

            String cikti = "";
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json_data = jArray.getJSONObject(i);
                cikti += json_data.getString("isim") + " - " + json_data.getString("numara") + "\n";
            }
            TextView t = (TextView) findViewById(R.id.textView);
            t.setText(cikti);
        }catch (Exception e) {

        }
    }





    private class ConnectionTask extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... params) {
            if(params[0] == "ekle") {
                postGonder(params[1],params[2]);
                return JSONVerileriniOku();
            }else {
                return JSONVerileriniOku();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            // doInBackground tammalandıktan sonra çalışan fonksiyon
            JSONGoster(result);
        }



        protected void postGonder(String isim, String numara) {
            try{
                URL urlToRequest = new URL("http://mutlakyazilim.com/webService.php?islem=ekle");
                HttpURLConnection urlConnection = (HttpURLConnection) urlToRequest.openConnection();

                urlConnection.setConnectTimeout(3000);
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                //send the POST out
                PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
                out.print("isim="+isim+"&numara="+numara);
                out.close();


                //Sayfadan dönen sonuç = gelenVeri
                InputStream aInputStream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedInputStream aBufferedInputStream = new BufferedInputStream(aInputStream);

                StringBuilder sb = new StringBuilder();
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(aBufferedInputStream));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                String gelenVeri = sb.toString();
                System.out.println(gelenVeri);

            }catch (Exception e) {

            }

        }


        protected String JSONVerileriniOku() {
            String gelenVeri="";
            try {
                URL aURL = new URL("http://mutlakyazilim.com/webService.php");

                /* Open a connection to that URL. */
                final HttpURLConnection aHttpURLConnection = (HttpURLConnection) aURL.openConnection();

                /* Define InputStreams to read from the URLConnection. */
                InputStream aInputStream = aHttpURLConnection.getInputStream();
                BufferedInputStream aBufferedInputStream = new BufferedInputStream(aInputStream);


                StringBuilder sb = new StringBuilder();
                String line;

                BufferedReader br = new BufferedReader(new InputStreamReader(aBufferedInputStream));

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                gelenVeri = sb.toString();

            } catch (IOException e) {

            }
            return gelenVeri;
        }

    }
}
